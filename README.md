# Desktop application: JavaFX
This is the repository for the desktop application.

## Useful links
Cloud repo: https://bitbucket.org/roelvandewiel/desktop_applicatie_submarine_cloud/src/master/ (ANY CHANGES IN THE CLOUD REPO WILL BE OVERWRITTEN, AND THUS REMOVED)

Sonar: https://sonarcloud.io/dashboard?id=Roel-van-de-Wiel_desktop_applicatie_submarine_cloud
